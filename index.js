const express = require('express');
const logger = require('./src/config/logger/logger');

const app = express();

app.use('/', express.static('./src' + '/'));
const port = 8080;
app.listen(port, () => {
  logger.info(`Servidor Rodando na porta:  ${port}`);
});
