import moviesMocks from '../mocks/moviesData.js';
import sessionMocks from '../mocks/sessionsData.js';
import roomsMocks from '../mocks/roomsInfoData.js';
import moviesSessionMocks from '../mocks/moviesSessions.js';
export default class LandingPageController{
  
  constructor(){
    this.movies = moviesMocks;
    this.rooms = roomsMocks;
    this.session = sessionMocks;
    this.moviesSessionInfo = moviesSessionMocks;
  }

  getMovies(){
    return this.movies;
  }

  getRooms(){
    return this.rooms;
  }

  getSessions(){
    return this.session;
  }

  getMoviesSessionInfo(){
    return this.moviesSessionInfo;
  }

}
