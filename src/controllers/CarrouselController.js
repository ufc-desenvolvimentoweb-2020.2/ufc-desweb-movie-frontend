import mocks from '../mocks/moviesData.js';
export default class CarrouselController{
  
  constructor(){
    this.movies = mocks;
  }

  getMovies(){
    return this.movies;
  }

}
