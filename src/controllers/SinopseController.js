import mocks from '../mocks/moviesData.js';
export default class SinopseController{
  
  constructor(){
    this.movies = mocks;
  }

  getMovies(){
    return this.movies;
  }

  findMovie(movieId){
    return this.movies.find((movie) => {
      return movieId == movie.id;
    });
  }

}
