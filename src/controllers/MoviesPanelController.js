import mocks from '../mocks/moviesData.js';
export default class MoviesPanelController{
  
  constructor(){
    this.movies = mocks;
    console.log(this.movies);
  }

  getMovies(){
    return this.movies;
  }

}
