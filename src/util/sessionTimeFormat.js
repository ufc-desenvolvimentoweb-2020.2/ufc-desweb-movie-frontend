export function hourFormat (time) {
  console.log('time: '+time);
  if (time && time.length > 0) {
    const stepTime = typeof time.split('T')[1] !== 'undefined' ? time.split('T')[1] : '';
    const hour = `${stepTime.split(':')[0]}:${stepTime.split(':')[1]}`;
    //console.log(hour);
    return hour;
  } else {
    return '';
  }
};