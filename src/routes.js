import React from 'react';
import {BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import Landing from './views/pages/LandingPage';
import SeatChoosing from './views/pages/SeatChoosing';
import MoviesPanel from './views/pages/MoviesPanel';
import PaymentPage from './views/pages/PaymentPage';
import Sinopse from './views/pages/Sinopse';
import EditPage from './views/pages/EditPage';
import Login from './views/pages/Login';
import Register from './views/pages/Register';

const isAutenticated = () => {
    const token = localStorage.getItem('authorization');
    if (token) {
      return true;
    }
    return false;
}

function CustomRoute({ isPrivate, ...rest }) {

    if (isPrivate && !isAutenticated()) {
      return <Redirect to="/login" />
    }

    return <Route {...rest} />;
}

export default function Routes () {
    return (
        <BrowserRouter>
            <Switch>
                {/* <CustomRoute isPrivate path="/" exact component={Landing} /> */}
                <CustomRoute  path="/" exact component={Landing} />
                <CustomRoute isPrivate path="/seatchoosing" exact component={SeatChoosing}/>
                {/* <CustomRoute isPrivate path="/moviespanel" exact component={MoviesPanel}/> */}
                <CustomRoute  path="/moviespanel" exact component={MoviesPanel}/>
                <CustomRoute isPrivate path="/paymentcard" exact component={PaymentPage}/>
                {/* <CustomRoute isPrivate path="/sinopse" exact component={Sinopse}/> */}
                <CustomRoute  path="/sinopse" exact component={Sinopse}/>
                <CustomRoute path="/login" exact component={Login}/>
                <CustomRoute isPrivate path="/edit" exact component={EditPage}/>
                <CustomRoute path="/register" exact component={Register}/>
            </Switch>
        </BrowserRouter>
    );
}