const {MongoClient} = require('mongodb');
const { cli } = require('winston/lib/winston/config');

const uri = "mongodb://admin:admin@localhost:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";

// Database Name
const dbName = 'admin';

const insertDocuments = function(db, callback) {
  // Get the documents collection
  const collection = db.collection('documents');
  // Insert some documents
  collection.insertMany([
    {a : 1}, {a : 2}, {a : 3}
  ], function(err, result) {
    console.log("Inserted 3 documents into the collection");
    callback(result);
  });
}

const findDocuments = function(db, callback) {
  // Get the documents collection
  const collection = db.collection('movies');
  // Find some documents
  collection.find({}).toArray(function(err, docs) {
    console.log("Found the following records");
    console.log(docs)
    callback(docs);
  });
}

// // Use connect method to connect to the server
// MongoClient.connect(uri, function(err, client) {
//   console.log("Connected successfully to server");

//   const db = client.db(dbName);

//   findDocuments(db, function() {
//     client.close();
//   });
// });

const findAllMovies = async() => {
  const client = new MongoClient(uri);
  await client.connect();
  const db = await client.db(dbName);
  const collection =  await db.collection('movies');
  console.log(collection.indexes);
  return await collection.find({}).toArray();
}

findAllMovies().then((e) => {
  console.log(e);
});
