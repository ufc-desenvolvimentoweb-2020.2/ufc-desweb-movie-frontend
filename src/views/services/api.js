import axios from 'axios';

/* const URL = process.env.URL || 'http://localhost';

const PORT = process.env.PORT || 8080; */

const URL = 'http://localhost';

const PORT = '8080';

const api = axios.create({
    baseURL: `${URL}:${PORT}`
});

export default api;