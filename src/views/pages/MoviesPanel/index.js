import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import './style.css';

import NavyMain from '../../components/NavyMain';
import FooterMain from '../../components/FooterMain';
import Carousel from '../../components/Carousel';

import MoviesPanelController from '../../../controllers/MoviesPanelController';
import api from '../../services/api';



export default function MoviesPanel () {

    const history = useHistory();
    
    const
        [styleMode, setStyleMode] = useState(''),
        [fontSize, setFontSize] = useState(''),
        [movies, setMovies] = useState([]);
    
    useEffect( () => {
     setStyleMode(localStorage.getItem('style-mode') || '');
     setFontSize(localStorage.getItem('font-size') || '');
     
     const authorization = localStorage.getItem('authorization');

     api.get('/movie', {
        headers: {
            'x-access-token': authorization
        }
    }).then(response => {
        setMovies(response.data);
    }).catch (e => {
        history.push('/login');
    });

    },[]);
    return(
        <>
            <NavyMain
                setStyleMode={setStyleMode}
                setFontSize={setStyleMode}
            />
                <div className="movies-panel">
                    <section className="carrousel">
                        {movies && movies.length > 0 ?
                            <Carousel
                                movies={movies}
                            />
                            :
                            ''
                        }
                    </section>
                </div>
            <FooterMain
                styleMode={styleMode}
                fontSize={fontSize}
            />
        </>
    );
}