import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import api from '../../services/api';
import { useHistory } from 'react-router-dom';
import './style.css';


export default function Login({ setToken }) {

  const history = useHistory();

  const
    [styleMode, setStyleMode] = useState(''),
    [fontSize, setFontSize] = useState('');

  useEffect( () => {
    setStyleMode(localStorage.getItem('style-mode') || '');
    setFontSize(localStorage.getItem('font-size') || '');
  });

  const [username, setUserName] = useState();
  const [password, setPassword] = useState();

  function validateForm() {
    return username.length > 0 && password.length > 0;
  }

  const handleSubmit = async e => {
    e.preventDefault();

    api.post('/login', {
      "user": username,
      "password": password
    }).then(response => {

      if (response.data.auth) {

        localStorage.setItem('authorization', response.data.token);

        history.push('/');

    } else {

        alert('Não foi possível logar. Tente novamente.');

    }
      console.log(response.data);
    }).catch (e => {
      history.push('/login');
    });
  }

  return(
    <div className={`login-container${styleMode}${fontSize}`}>
      <div className="login-wrapper">
        <div className="image-wrapper">
          <img src="assets/logo.png" alt="logo"/>
        </div>
        <form onSubmit={handleSubmit}>
          <label>
            <input className="form-field" type="text" placeholder="email" onChange={e => setUserName(e.target.value)} />
          </label>
          <label>
            <input className="form-field" type="password" placeholder="senha" onChange={e => setPassword(e.target.value)} />
          </label>
          <button className="form-field" type="submit">Logar</button>
        </form>
      </div>
    </div>
  )
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
};