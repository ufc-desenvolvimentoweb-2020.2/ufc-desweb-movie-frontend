import React, {useEffect ,useState} from 'react';
import { useHistory } from 'react-router-dom';

import './style.css';

import NavyMain from '../../components/NavyMain';
import FooterMain from '../../components/FooterMain';
import Carousel from '../../components/Carousel';
import LandingPageController from '../../../controllers/LandingPageController';
import api from '../../services/api';
import {hourFormat} from '../../../util/sessionTimeFormat';


export default function Logon () {

    const history = useHistory();

    const
        [styleMode, setStyleMode] = useState(''),
        [fontSize, setFontSize] = useState(''),
        [moviesSessionInfo, setMoviesSessionInfo] = useState([]),
        [movies, setMovies] = useState([]);

    useEffect( () => {
      setStyleMode(localStorage.getItem('style-mode') || '');
      setFontSize(localStorage.getItem('font-size') || '');

      const authorization = localStorage.getItem('authorization');
      
        api.get('/movie',{
            headers: {
                'x-access-token': authorization
            }
        }).then(response => {
            setMovies(response.data);
        }).catch (e => {
            history.push('/login');
        });

        api.get('/landing-page-movies',{
            headers: {
                'x-access-token': authorization
            }
        }).then(response => {
            setMoviesSessionInfo(response.data);
            console.log(response.data);
        }).catch (e => {
            history.push('/login');
        });

    },[]);

    const controller = new LandingPageController();
    //const moviesSessionInfo = controller.getMoviesSessionInfo();

    return( 
        <>
            <NavyMain
                setStyleMode={setStyleMode}
                setFontSize={setStyleMode}
            />
            <div className={`main-frame${styleMode}${fontSize}`}>
                <div className="content">
                    <div className="movie-page-navi">
                        <div className="cinema-selection">
                            <label htmlFor="cinemas">Selecione o cinema: </label>
                            <select name="cinemas" id="cinemas">
                                <option value="ufcine">UFCine</option>
                            </select>
                        </div>
                        <div className="day-selection">
                            <div className="today">
                                <p>Hoje</p>
                                <p>30/01</p>
                            </div>
                            <div className="day-month">
                                <p>Dom</p>
                                <p>31/01</p>
                            </div>
                            <div className="day-month">
                                <p>Seg</p>
                                <p>01/02</p>
                            </div>
                            <div className="day-month">
                                <p>Ter</p>
                                <p>02/02</p>
                            </div>
                            <div className="day-month">
                                <p>Quar</p>
                                <p>03/02</p>
                            </div>
                            <div className="day-month">
                                <p>Quin</p>
                                <p>04/02</p>
                            </div>
                        </div>
                    </div>
                    <div className="main-panel">
                        <div className="left-panel">
                            <img src="assets/mulher-maravilha-1984.jpg" alt="mulher maravilha 1984"/>
                            <div className="sinopse">SINOPSE &gt;</div>
                        </div>
                        <div className="right-panel">
                            {movies && movies.length > 0 ?
                                <Carousel
                                    movies={movies}
                                />
                                :
                                ''
                            }
                            <div className="movies-info">
                                <ul className="infos-list">
                                    
                                    {
                                        moviesSessionInfo.map( (sessionInfo) => {
                                            return (
                                                <li className="movie-iten">
                                                <div className="movie-places"> 
                                                <div className="movie-title">
                                                    <span className="title">
                                                        {sessionInfo.title}
                                                    </span>
                                                    <span className="class-12">
                                                        12
                                                    </span>
                                                    <ul className="movie-rooms">
                                                        {
                                                             sessionInfo.rooms.map( (room) => {
                                                                    return (<li>
                                                                        <div className="room-info">
                                                                        <div className="left-info">
                                                                            <div className="room-name">
                                                                                {room.name}
                                                                            </div>
                                                                            <div className="movie-dub">
                                                                                DUB
                                                                            </div>
                                                                            <div className="room-type">
                                                                                UFCINEMA
                                                                            </div>
                                                                        </div>
                                                                        <div className="room-hours">
                                                                            <ul>
                                                                                {
                                                                                    room.sessions.map((session, index) => {
                                                                                        return (
                                                                                        <li id={index} className="hour-off">
                                                                                            {hourFormat(session.start_time)}
                                                                                        </li>);
                                                                                    })
                                                                                }
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    </li> 
                                                                    )
                                                            }) 
                                                        }
                                                    </ul>
                                                </div>
                                                </div>
                                                </li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <FooterMain
                styleMode={styleMode}
                fontSize={fontSize}
            />
        </>
    );
}