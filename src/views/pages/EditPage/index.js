//  por enquanto só copiei a página do MoviesPanel, e ainda não ajeitei
import React, { useState, useEffect } from 'react';

import './style.css';

import NavyMain from '../../components/NavyMain';
import FooterMain from '../../components/FooterMain';
import Carousel from '../../components/Carousel';

import MoviesPanelController from '../../../controllers/MoviesPanelController';
import api from '../../services/api';



export default function EditPage () {
    /* const movies =  [];
    const controller = new MoviesPanelController();
    const baseURL = 'http://image.tmdb.org/t/p/w185';
    for(let movie of controller.getMovies()){
        movies.push(
        <li>
            <img src={`${baseURL}${movie.backdrop_path}`} alt={`${movie.original_title}`}/>
        </li>);
    } */

    const
        [styleMode, setStyleMode] = useState(''),
        [fontSize, setFontSize] = useState(''),
        [movies, setMovies] = useState([]);
    
    useEffect( () => {
     setStyleMode(localStorage.getItem('style-mode') || '');
     setFontSize(localStorage.getItem('font-size') || '');
     
     const authorization = localStorage.getItem('authorization');

     try {
        api.get('/movie', {
            headers: {
                'x-access-token': authorization
            }
        }).then(response => {
            setMovies(response.data);
        })
     } catch (e) {
        console.log(e);
     }

    },[]);
    return(
        <>
            <NavyMain
                setStyleMode={setStyleMode}
                setFontSize={setStyleMode}
            />
                <div className="movies-panel">
                    <section className="carrousel">
                        {movies && movies.length > 0 ?
                            <Carousel
                                movies={movies}
                            />
                            :
                            ''
                        }
                    </section>
                </div>
            <FooterMain
                styleMode={styleMode}
                fontSize={fontSize}
            />
        </>
    );
}