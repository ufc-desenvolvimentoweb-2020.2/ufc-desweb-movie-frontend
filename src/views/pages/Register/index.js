import React, { useState, useEffect } from 'react';
import './style.css';
import { Link, useHistory } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";
import api from '../../services/api';

import NavyMain from '../../components/NavyMain';
import FooterMain from '../../components/FooterMain';

export default function Register () {

 const 
    [user, setUser] = useState(''),
    [password, setPassword] = useState('');

 const history = useHistory();

 const previousPage = localStorage.getItem('previousPage');

 const
    [styleMode, setStyleMode] = useState(''),
    [fontSize, setFontSize] = useState('');

 useEffect( () => {
  setStyleMode(localStorage.getItem('style-mode') || '');
  setFontSize(localStorage.getItem('font-size') || '');
 });

 async function HandleRegister (event) {

    event.preventDefault();

    const data = {
        username:user,
        password
    };

    try {
        await api.post('/client', data);
        if (previousPage) {
            history.push(previousPage);
        } else {
            history.push('/');
        }
        
    } catch (err) {

        alert(`Error when trying to save user. Try again.`);

    }
 }

 return (
    <>
        <NavyMain
            setStyleMode={setStyleMode}
            setFontSize={setStyleMode}
        />
            <div className={`register-container${styleMode}${fontSize}`}>

                <div className="content">

                    <section>

                    <img src="assets/logo.png" alt="Sistema Internos"/>

                    <h1>Registro de Usuário</h1>

                    <p>
                        Preencha os campos ao lado para ter um novo usuário cadastrado.
                    </p>

                    <Link className="back-link" to={previousPage}>
                        <FiArrowLeft size={16} color="gray" />
                        <span>Back</span>
                    </Link>

                    </section>

                    <form onSubmit={HandleRegister}>

                       

                    <input 
                        placeholder="Login"
                        value={user}
                        onChange={ e=> setUser(e.target.value)}
                        />

                    <input
                        type = "password"
                        placeholder="Senha"
                        value={password}
                        onChange={ e=> setPassword(e.target.value)}
                        />

                    <button className="button" type="submit">
                        Registrar
                    </button>

                    </form>

                </div>

            </div>
        <FooterMain
            styleMode={styleMode}
            fontSize={fontSize}
        />
    </>
 );
}