import React, { useState, useEffect } from 'react';
import './style.css';

import NavyMain from '../../components/NavyMain';
import FooterMain from '../../components/FooterMain';

export default function SeatChoosing () {

	const
		[styleMode, setStyleMode] = useState(''),
		[fontSize, setFontSize] = useState('');

	useEffect( () => {
		setStyleMode(localStorage.getItem('style-mode') || '');
		setFontSize(localStorage.getItem('font-size') || '');
	});

    return(
    <div className="seat-choosing-panel">
        <NavyMain
          setStyleMode={setStyleMode}
          setFontSize={setStyleMode}
        />
        <div className="painel-escolha-assentos">
					<div className="painel-titulo-assento">
						<div>Escolha seu Assento</div>
					</div>

					<table id="assentos">
						<tbody>
							<tr>
								<th scope="col">&nbsp;&nbsp;&nbsp;</th>
								<th scope="col">1</th>
								<th scope="col">2</th>
								<th scope="col">3</th>
								<th scope="col">4</th>
								<th scope="col"></th>
								<th scope="col">5</th>
								<th scope="col">6</th>
								<th scope="col">7</th>
								<th scope="col">8</th>
							</tr>

							<tr>
								<td>E</td>
								<td></td>
								<td>
									<span className="assento">E2</span>
								</td>
								<td>
									<span className="assento">E3</span>
								</td>
								<td>
									<span className="assento ocupado">E4</span>
								</td>
								<td></td>
								<td>
									<span className="assento">E5</span>
								</td>
								<td>
									<span className="assento">E6</span>
								</td>
								<td>
									<span className="assento">E7</span>
								</td>
								<td>
									<span className="assento">E8</span>
								</td>
							</tr>
						</tbody>
						<tr>
							<td>D</td>
							<td></td>
							<td>
								<span className="assento">D2</span>
							</td>
							<td>
								<span className="assento">D3</span>
							</td>
							<td>
								<span className="assento">D4</span>
							</td>
							<td></td>
							<td>
								<span className="assento ocupado">D5</span>
							</td>
							<td>
								<span className="assento">D6</span>
							</td>
							<td>
								<span className="assento ocupado">D7</span>
							</td>
							<td>
								<span className="assento">D8</span>
							</td>
						</tr>

						<tr>
							<td>C</td>
							<td></td>
							<td>
								<span className="assento">C2</span>
							</td>
							<td>
								<span className="assento">C3</span>
							</td>
							<td>
								<span className="assento ocupado">C4</span>
							</td>
							<td></td>
							<td></td>
							<td>
								<span className="assento">C6</span>
							</td>
							<td>
								<span className="assento ocupado">C7</span>
							</td>
							<td></td>
						</tr>

						<tr>
							<td><br></br></td>
						</tr>

						<tr>
							<td>B</td>
							<td>
								<span className="assento">B1</span>
							</td>
							<td>
								<span className="assento">B2</span>
							</td>
							<td>
								<span className="assento">B3</span>
							</td>
							<td>
								<span className="assento">B4</span>
							</td>
							<td></td>
							<td>
								<span className="assento">B5</span>
							</td>
							<td>
								<span className="assento">B6</span>
							</td>
							<td>
								<span className="assento ocupado">B7</span>
							</td>
							<td>
								<span className="assento">B8</span>
							</td>
						</tr>

						<tr>
							<td>A</td>
							<td>
								<span className="assento">A1</span>
							</td>
							<td>
								<span className="assento">A2</span>
							</td>
							<td>
								<span className="assento">A3</span>
							</td>
							<td>
								<span className="assento">A4</span>
							</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td>
								<span className="assento">A5</span>
							</td>
							<td>
								<span className="assento">A6</span>
							</td>
							<td>
								<span className="assento ocupado">A7</span>
							</td>
							<td>
								<span className="assento">A8</span>
							</td>
						</tr>
					</table>

					<div className="painel-legenda-assento">
						<div>Legendas</div>

						<div className="legenda-assento">
							<div className="linha-legenda">
								<span className="assento desativado"></span>
								<span className="legenda-label">Livre</span>
							</div>
							<div className="linha-legenda">
								<span className="assento ocupado desativado"></span>
								<span className="legenda-label">Ocupado</span>
							</div>
							<div className="linha-legenda">
								<span className="assento escolhido desativado"></span>
								<span className="legenda-label">Escolhido</span>
							</div>
						</div>
					</div>
				</div>
        <FooterMain
          styleMode={styleMode}
          fontSize={fontSize}
        />
    </div>
    );
}