import React, { useState, useEffect } from 'react';

import './style.css';

import NavyMain from '../../components/NavyMain';
import FooterMain from '../../components/FooterMain';
import SinopseController from '../../../controllers/SinopseController';
import { useLocation} from "react-router-dom";

const useQuery = () => {
    return new URLSearchParams(useLocation().search);
}

export default function Sinopse () {

    const
        [styleMode, setStyleMode] = useState(''),
        [fontSize, setFontSize] = useState('');

    useEffect( () => {
     setStyleMode(localStorage.getItem('style-mode') || '');
     setFontSize(localStorage.getItem('font-size') || '');
    });

    const controller = new SinopseController();
    const query = useQuery();
    const movieId = query.get("id");
    
    const movie = controller.findMovie(movieId);
    const baseURL = 'http://image.tmdb.org/t/p/w185';

    return (
        <>
        <NavyMain
          setStyleMode={setStyleMode}
          setFontSize={setStyleMode}
        />
            <div className="sinopse-panel">
                <section id= "sinopse">
                    <div className="container">
                        <div id="img">
                         <img src={`${baseURL}${movie.backdrop_path}`} alt={`${movie.original_title}`}/>                           
                        </div>
                        <div id="descriptioninfo">
                            <div id="description">
                                <h3> {movie.title}</h3>
                                <p>
                                {movie.overview}
                                </p>
                            </div>
                            <div id="info" >
                                <div id = "elenco" className="colunm-info">
                                    <h3> Data de Estreia </h3>
                                    <p>{movie.release_date}</p>
                                </div>
    
                                <div id = "genero" className="colunm-info">
                                    <h3> Gênero </h3>
                                    <p>{movie.genre_ids}</p>
                                </div>
                                <div id = "duracao" className="colunm-info">
                                    <h3> Duração </h3>
                                    <p> 2h 20 min</p>
                                </div>
                                <div id = "classificacao" className="colunm-info">
                                    <h3> Classificação </h3>
                                    <p> 12 Anos</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <FooterMain
                styleMode={styleMode}
                fontSize={fontSize}
            />
        </>
    );
}