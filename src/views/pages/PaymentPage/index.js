import React, { useState, useEffect } from 'react';

import './style.css';

import NavyMain from '../../components/NavyMain';
import FooterMain from '../../components/FooterMain';

export default function PaymentPage () {

    const
      [styleMode, setStyleMode] = useState(''),
      [fontSize, setFontSize] = useState('');
  
    useEffect( () => {
      setStyleMode(localStorage.getItem('style-mode') || '');
      setFontSize(localStorage.getItem('font-size') || '');
    });
    return(
        <div className="payment-panel">
            <NavyMain
                setStyleMode={setStyleMode}
                setFontSize={setStyleMode}
            />
            <div className={`painel${styleMode}${fontSize}`}>
                <div className="painel-head">
                    <h3> Detalhes do pagamento</h3>
                </div>
                <div className="painel-body">
                  <form action="pagamento">
                    <label for="numeroCartao">
                         Número do Cartão
                    </label>
                    <div>
                      <input type="text" placeholder="Número do Cartão"/>
                      <span className="glyphicon glyphicon-lock"></span>
                    </div>
                    <label for="validade"> Validade</label>
                    <div>
                      <input type="text" placeholder="MM/AAAA"/>
                    </div>
                    <label for="ccv">CCV</label>
                    <div>
                      <input type="text" placeholder="CCV"/>
                    </div>
                    <button 
                      className="btn btn-sucess btn-lg btn-block" 
                      type="submit" 
                      value="Pagar"
                    > 
                      Pagar 
                    </button>
                  </form>
                </div>
            </div>
            <FooterMain
                styleMode={styleMode}
                fontSize={fontSize}
            />
        </div>
    );
}