import React, { useState, useEffect } from "react";
import { useHistory} from 'react-router-dom';

import './style.css';

export default function NavyMain (props) {

  const
    [styleMode, setStyleMode] = useState(''),
    [fontSize, setFontSize] = useState(''),
    history = useHistory();

    useEffect( () => {
      setStyleMode(localStorage.getItem('style-mode') || '');
      setFontSize(localStorage.getItem('font-size') || '');
    });

    const handleAltoContrate = () => {
      if (styleMode === '') {
        setStyleMode(' high-contrast');
        props.setStyleMode(' high-contrast');
        localStorage.setItem('style-mode', ' high-contrast');
      } else {
        setStyleMode('');
        props.setStyleMode('');
        localStorage.setItem('style-mode', '');
      }
    }

    const handleUpFont = () => {
      if (fontSize === '') {
        setFontSize(' medium');
        props.setFontSize(' medium');
        localStorage.setItem('font-size', ' medium');
      } else if(fontSize === ' medium') {
        setStyleMode(' bigger');
        props.setStyleMode(' bigger');
        localStorage.setItem('font-size', ' bigger');
      }
    }

    const handleDownFont = () => {
      if (fontSize === ' bigger') {
        setFontSize(' medium');
        props.setFontSize(' medium');
        localStorage.setItem('font-size', ' medium');
      } else if(fontSize === ' medium') {
        setFontSize('');
        props.setFontSize('');
        localStorage.setItem('font-size', '');
      }
    }

    const handleHome = () => {
      history.push('/');
    }

    const handleFilmes = () => {
      history.push('/moviespanel');
    }

    const handleCadastro = () => {
      history.push('/register');
    }

    const handleLogin = () => {
      history.push('/login');
    }

    const handleLogout = () => {
      localStorage.removeItem('authorization');
      history.push('/');
    }

    const handleEdit = () => {
      history.push('/Edit');
    }

    return (
        <div className={`main-navi-content${styleMode}${fontSize}`}>
            <div className="access-bar">
              <ul>
                <li onClick={handleAltoContrate}>
                  Alto Contraste
                </li>
                <li onClick={handleUpFont}>
                  A+
                </li>
                <li onClick={handleDownFont}>
                  A-
                </li>
              </ul>
            </div>
            <div className="navbar">
                <div className="left-side-bar">
                  <div className="navbar-element">
                    <img src="../../assets/logo.png" alt="UFCine"/>
                  </div>
                  <ul className="navbar-element">
                    <li>
                      <a onClick={handleHome}>Home</a>
                    </li>
                    <li>
                      <a onClick={handleFilmes}>Filmes</a>
                    </li>
                    <li>
                      <a onClick={handleCadastro}>Cadastro</a>
                    </li>
                    {localStorage.getItem('authorization')?
                      <li>
                      <a onClick={handleLogout}>Logout</a>
                      </li>
                      :
                      <li>
                        <a onClick={handleLogin}>Login</a>
                      </li>
                    }
                    {localStorage.getItem('authorization')?
                      <li>
                      <a onClick={handleEdit}>Editar</a>
                      </li>
                      :
                      ''
                    }
                  </ul>
                </div>
                
            </div>
        </div>
    );
}