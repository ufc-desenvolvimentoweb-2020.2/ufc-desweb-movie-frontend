import React, { useState, useEffect } from "react";
import './style.css';

export default function FooterMain (props) {

    const
        [styleMode, setStyleMode] = useState(props.styleMode || ''),
        [fontSize, setFontSize] = useState(props.fontSize || '');

    useEffect( () => {
      setStyleMode(localStorage.getItem('style-mode') || '');
      setFontSize(localStorage.getItem('font-size') || '');
    });

    return (
        <div className={`main-footer-content${styleMode}${fontSize}`}>
            <section id = "bottom">
                <h1>UFCINE</h1>
                <div className = "container">
                    <div id="bot" className="text-left">
                        <div className="row">
                            <div className="columnBottom">
                                <h3>Onde Nos Encontrar</h3>
                                <ul>
                                    <li>
                                        <a 
                                        className="bottom" 
                                        href="https://www.google.com/maps/place/Bloco+725+-+Departamento+de+Engenharia+de+Teleinform%C3%A1tica+-+DETI+%2F+UFC/@-3.7449195,-38.5781234,15z/data=!4m5!3m4!1s0x0:0xc206c4cb5d4e7438!8m2!3d-3.7449195!4d-38.5781234" 
                                        target="_blank"
                                        >
                                            UFC-DETI
                                        </a>
                                    </li>
                                    <li>
                                        <a 
                                        className="facebook" 
                                        href="https://www.facebook.com/UFCinforma/" 
                                        target="_blank"
                                        >
                                            Facebook
                                        </a>
                                    </li>
                                    <li>
                                        <a 
                                        className="instagram" 
                                        href="https://www.instagram.com/ufcinforma/?hl=pt-br" 
                                        target="_blank"
                                        >
                                            Instagram
                                        </a>
                                    </li>
                                    <li>
                                        <a 
                                        className="twitter" 
                                        href="https://twitter.com/UFCinforma" 
                                        target="_blank"
                                        >
                                            Twitter
                                        </a>
                                    </li>
                                    <li>
                                        <a 
                                        className="youtube"
                                        href="https://www.youtube.com/channel/UCY85wHK1Z3LJyKB7XQ-pJ0g" 
                                        target="_blank"
                                        >
                                            Youtube
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="columnBottom">
                                <h3>Ingressos</h3>
                                <ul>
                                    <li>
                                        <a className="bottom" href="javascript:void(0)" >Preços</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="columnBottom">
                                <h3>Filmes</h3>
                                <ul>
                                    <li>
                                        <a className="bottom" href="javascript:void(0)" >Em Cartaz</a>
                                    </li>
                                    <li>
                                        <a className="bottom" href="javascript:void(0)">Estreias da Semana</a> 
                                    </li>
                                    <li>
                                        <a className="bottom" href="javascript:void(0)" >Próximos Lançamentos</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="columnBottom">
                                <h3>Empresa</h3>
                                <ul>
                                    <li>
                                        <a 
                                          className="bottom" 
                                          href="https://gitlab.com/ufc-desenvolvimentoweb-2020.2/ufc-desweb-movie-frontend" 
                                          target="_blank"
                                        >
                                            UFCine
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <footer id="footer">
                <div className="container">
                    <div className="row text-center">
                        © 2020 <a 
                        className="ufcine" 
                        href="https://gitlab.com/ufc-desenvolvimentoweb-2020.2/ufc-desweb-movie-frontend"
                        >
                            UFCine
                        </a>
                    </div>
                </div>
            </footer>
            </section>


        </div>
    );
}