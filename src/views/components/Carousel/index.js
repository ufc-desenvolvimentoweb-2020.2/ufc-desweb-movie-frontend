import React from 'react';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';
import { withRouter } from "react-router-dom";
import CarrouselController from '../../../controllers/CarrouselController';

import './style.css';

class Carousel extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      translated: 0,
      totalSpace: 0,
      movies: this.props.movies
    };
  }

  componentDidMount() {
    const
      carouselWidth = document.querySelector('.carousel').offsetWidth,
      sliderWidth = document.querySelector('.slider').offsetWidth,
      totalSpace = sliderWidth - carouselWidth;
    this.setState({ ...this.state, totalSpace: totalSpace });
  }

  handlePrevEvent = () => {
    const newTranslated = this.state.translated + 250;
    if (newTranslated <= 0) {
      this.setState({...this.state, translated: newTranslated});
    }
  };

  handleNextEvent = () => {
    const newTranslated = this.state.translated - 250;
    if (Math.abs(this.state.translated) < this.state.totalSpace) {
      this.setState({...this.state, translated: newTranslated});
    } else {
      this.setState({...this.state, translated: this.state.totalSpace});
    }
  };

  render() {
    const baseURL = 'http://image.tmdb.org/t/p/w185';

    return(
      <div className="carousel-container">
        <div className="carousel">
          <ul className="slider" style={{transform: `translate(${this.state.translated}px)`}}>
            {this.state.movies ? this.state.movies.map((movie, index) => {
              return (
                <li id={index}>
                  <img src={`${baseURL}${movie.poster_path}`} alt={`${movie.original_title}`}/>
                </li>
              );
            }) : ''}
          </ul>
        </div>
        <div className="controls">
           <div 
            className="arrow prev" 
            style={this.state.translated === 0 ? {display: 'none'} : {display: 'flex'}} 
            onClick={this.handlePrevEvent}
           >
              <FiChevronLeft/>
            </div>
           <div 
            className="arrow next"
            style={Math.abs(this.state.translated) >= this.state.totalSpace ? {display: 'none'} : {display: 'flex'}} 
            onClick={this.handleNextEvent}
           >
             <FiChevronRight/>
           </div>
         </div>
      </div>
      
    )
  }
}

export default withRouter(Carousel);