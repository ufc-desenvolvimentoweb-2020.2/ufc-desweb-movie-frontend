class Session {

  constructor(room,movie,startTime,endTime){
    this.room = room;
    this.movie = movie;
    this.startTime = startTime;
    this.endTime = endTime;
  }
  
  get room(){
    return this.room;
  }

  get movie(){
    return this.movie;
  }

  get startTime(){
    return this.startTime;
  }

  get endTime(){
    return this.room;
  }
  
}

module.exports = Session;