class Ticket {
  constructor(user, type, session, seat){
    this.user = user;
    this.type = type;
    this.session = session;
    this.seat = seat;
  }

  get user() {
    return this.user;
  }

  get type() {
    return this.type;
  }

  get session() {
    return this.session;
  }

  get seat() {
    return this.seat;
  }
}

module.exports = Ticket;