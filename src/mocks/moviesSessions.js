module.exports = [
  {
    "id": 1,
    "title": "Tenet",
    "rooms": [
      {
        "id": 1,
        "name": "Sala 01",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 1,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 1,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
      {
        "id": 1,
        "name": "Sala 02",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 2,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 2,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
      {
        "id": 1,
        "name": "Sala 03",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 3,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 3,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
    ]
  },
  {
    "id": 2,
    "title": "Jogos Vorazes",
    "rooms": [
      {
        "id": 1,
        "name": "Sala 01",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 1,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 1,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
      {
        "id": 1,
        "name": "Sala 02",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 2,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 2,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
      {
        "id": 1,
        "name": "Sala 03",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 3,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 3,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
    ]
  },
  {
    "id": 3,
    "title": "Jogos Vorazes: Em Chamas",
    "rooms": [
      {
        "id": 1,
        "name": "Sala 01",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 1,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 1,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
      {
        "id": 1,
        "name": "Sala 02",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 2,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 2,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
      {
        "id": 1,
        "name": "Sala 03",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 3,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 3,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
    ]
  },
  {
    "id": 4,
    "title":"Jogos Vorazes: A Esperança - Parte 1",
        "rooms": [
      {
        "id": 1,
        "name": "Sala 01",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 1,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 1,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
      {
        "id": 1,
        "name": "Sala 02",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 2,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 2,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
      {
        "id": 1,
        "name": "Sala 03",
        "capacity": 30,
        "sessions": [
          {
            "id": 1,
            "movieId": 3,
            "roomId": 1,
            "startTime": "10:00",
            "endTime": "12:00"
          },
          {
            "id": 2,
            "movieId": 3,
            "roomId": 1,
            "startTime": "13:00",
            "endTime": "12:00"
          }
        ]
      },
    ]
  }
];
