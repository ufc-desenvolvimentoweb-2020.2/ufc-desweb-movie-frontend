module.exports = [
  {
    "id": 1,
    "name": "Sala 01",
    "capacity": 30,
    "sessions": [
      {
        "id": 1,
        "movieId": 1,
        "roomId": 1,
        "startTime": "2020-12-12 10:00:00",
        "endTime": "2020-12-12 12:00:00"
      },
      {
        "id": 2,
        "movieId": 1,
        "roomId": 1,
        "startTime": "2020-12-12 13:00:00",
        "endTime": "2020-12-12 12:00:00"
      },
      {
        "id": 3,
        "movieId": 2,
        "roomId": 1,
        "startTime": "2020-12-12 18:00:00",
        "endTime": "2020-12-12 20:00:00"
      },
      {
        "id": 4,
        "movieId": 3,
        "roomId": 1,
        "startTime": "2020-12-12 21:00:00",
        "endTime": "2020-12-12 23:00:00"
      }
    ]
  },
  {
    "id": 2,
    "name": "Sala 02",
    "capacity": 25 ,
    "sessions": [
      {
        "id": 5,
        "movieId": 2,
        "roomId": 2,
        "startTime": "2020-12-12 11:00:00",
        "endTime": "2020-12-12 13:00:00"
      },
      {
        "id": 6,
        "movieId": 2,
        "roomId": 2,
        "startTime": "2020-12-12 14:00:00",
        "endTime": "2020-12-12 14:00:00"
      },
    ]
  },
  {
    "id": 3,
    "name": "Sala 03",
    "capacity": 40 ,
    "sessions": [
      {
        "id": 9,
        "movieId": 4,
        "roomId": 3,
        "startTime": "2020-12-12 10:30:00",
        "endTime": "2020-12-12 12:30:00"
      },
      {
        "id": 10,
        "movieId": 1,
        "roomId": 3,
        "startTime": "2020-12-12 13:30:00",
        "endTime": "2020-12-12 15:30:00"
      }
    ]
  },
  {
    "id": 4,
    "name": "Sala 04",
    "capacity": 50  
  }
];