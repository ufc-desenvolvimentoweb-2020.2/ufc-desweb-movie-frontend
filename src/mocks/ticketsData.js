module.exports = [
  {
    "id": 1,
    "userId": 1,
    "type": "MEIA",
    "sessionId": 1,
    "seat": 30
  },
  {
    "id": 2,
    "userId": 2,
    "type": "INTEIRA",
    "sessionId": 1,
    "seat": 30
  },
  {
    "id": 3,
    "userId": 3,
    "type": "MEIA",
    "sessionId": 2,
    "seat": 30
  },
  {
    "id": 4,
    "userId": 1,
    "type": "MEIA",
    "sessionId": 5,
    "seat": 30
  },
  {
    "id": 5,
    "userId": 3,
    "type": "MEIA",
    "sessionId": 3,
    "seat": 30
  },
  {
    "id": 6,
    "userId": 2,
    "type": "INTEIRA",
    "sessionId": 4,
    "seat": 30
  }
];